using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_LV7
{
    class Program
    {
        static void Main(string[] args)
        {
            //Main1();
            //Main2();
            Main34();
        }

        static void Main1() // Main za 1. zadatak
        {
            double[] array = new double[] { 1.2, 2.5, 5.1, 1, 12, 2, 17, 0, 3, 5, 1.1 };
            NumberSequence seqeunce = new NumberSequence(array);
            CombSort combSort = new CombSort();
            BubbleSort bubbleSort = new BubbleSort();
            SequentialSort sequentialSort = new SequentialSort();

            Console.Write("Prije sortiranja:\n" + seqeunce.ToString());

            //seqeunce.SetSortStrategy(combSort);
            //seqeunce.SetSortStrategy(bubbleSort);
            seqeunce.SetSortStrategy(sequentialSort);

            seqeunce.Sort();
            Console.WriteLine("Poslije sortiranja:\n" + seqeunce.ToString());
        }
        static void Main2()
        {
            double[] array = new double[] { 1.2, 2.5, 5.1, 1, 12, 2, 17, 0, 3, 5, 1.1 };
            NumberSequence seqeunce = new NumberSequence(array);
            LinearSearch linearSearch = new LinearSearch();

            Console.WriteLine("Niz:\n" + seqeunce.ToString());

            seqeunce.SetSearchStrategy(linearSearch);
            Console.WriteLine(seqeunce.Search(1));
        }
        static void Main34()
        {
            SystemDataProvider dataProvider = new SystemDataProvider();
            ConsoleLogger consoleLogger = new ConsoleLogger();
            dataProvider.Attach(consoleLogger);

            while (0 < 1)
            {
                dataProvider.GetCPULoad();
                dataProvider.GetAvailableRAM();
                System.Threading.Thread.Sleep(1000);
            }
        }
    }
}
